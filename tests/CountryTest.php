<?php

require_once __DIR__ . '/../src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../src/Country.php");

class CountryTest extends \PHPUnit\Framework\TestCase
{
   public function testGetCountryNameBySpirit()
   {
       $country = new Country();
       $result = $country->getCountryNameBySpirit("seum");
       $expected = 'Belgique';
       $this->assertTrue($result == $expected);
   }

   public function testGetCountryNameBySpirit2()
      {
          $country = new Country();
          $result = $country->getCountryNameBySpirit("pasleseum");
          $expected = 'France';
          $this->assertTrue($result == $expected);
      }
}
