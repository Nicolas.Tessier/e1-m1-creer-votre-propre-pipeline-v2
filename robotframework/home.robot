*** Settings ***
Resource         resources/common.resource
Suite Setup      Open browser to home page
Suite Teardown   Close browser

*** Test Cases ***
Check Home page
    Wait Until Page Contains    CREATIVE DESIGNERS!
    Execute JavaScript  window.scrollTo(0,1000)
    Wait Until Element is visible   xpath://*[@id="about"]/div[1]/div/div/div[1]/div/h2
