*** Settings ***
Resource         resources/common.resource
Suite Setup      Open browser to hello page
Suite Teardown   Close browser

*** Test Cases ***
Check hello page
    Wait Until Page Contains    Hello
